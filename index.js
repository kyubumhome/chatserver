const WebSocket = require("ws");
const server = new WebSocket.Server({ port: 3000 });

const users = [];

function broadcast(message) {
  var data = {
    type: 'message',
    message: message,
  }
  server.clients.forEach((client) => {
    if (client.readyState === WebSocket.OPEN) {
      client.send(JSON.stringify(data));
    };
  });
}

function refreshList() {
  server.clients.forEach(function each(client) {
    client.send(JSON.stringify({ type: 'list', list: users }));
  });
}

function userLeave(userId) {
  const userIndex = users.indexOf(userId);
  if (userIndex >= 0) users.splice(userIndex, 1);
  broadcast(userId + '님이 채팅방을 나가셨습니다.');
  refreshList();
}

// 연결이 수립되면 클라이언트에 메시지를 전송하고 클라이언트로부터의 메시지를 수신한다
server.on("connection", function(client) {
  client.on("message", function(data) {
    var object = JSON.parse(data);

    if (object.login) {
      users.push(object.id);
      client.id = object.id;
      broadcast(object.id + "님이 채팅방에 입장하셨습니다.");
      refreshList(client);
      return;
    }

    broadcast(object.id + ': ' + object.message);
  });

  client.isAlive = true;

  client.on('pong', function() {
    this.isAlive = true
  });

  client.on('close', function() {
    console.log(client.id, 'leave');
    userLeave(client.id);
  });
});

setInterval(function ping() {
  server.clients.forEach(function each(client) {
    if (client.isAlive === false) {
      userLeave(client.id);
      return client.terminate();
    }

    client.isAlive = false;
    client.ping('', false, true);
  });
  refreshList();
}, 5000);
